package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { (a: Int) =>
    println("min1 >  ")
    val h = insert(a, empty)
    findMin(h) == a
  }
  lazy val genHeap: Gen[H] =
    for {
      v <- arbitrary[Int]
      m <- oneOf(const(empty), genHeap)
    } yield insert(v, m)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("min2ElementosEnCola") = forAll { (a: Int, b: Int) =>
    println("min2ElementosEnCola >  ")
    val h = insert(a, insert(b, empty))
    findMin(h) == getMinValue(a, b)

  }

  property("genListaConUnElemetoYVolverlaVacia") = forAll { a: Int =>
    println("genListaConUnElemetoYVolverlaVacia >  ")
    isEmpty(deleteMin(insert(a, empty)))
  }

  property("genUnHeapYObtenerListaOrdenada") = forAll { (listInt: List[Int]) =>
    println("genUnHeapYObtenerListaOrdenada >  " + listInt)
    val h = createHeap(listInt, empty)
    val listaOrdporHeap = createOrderListByHeap(h)
    listaOrdporHeap.sorted == listaOrdporHeap
  }

  property("genObtenerMinimoValHep") = forAll { (h1: H, h2: H) =>
    println("genObtenerMinimoValHep >  ")
    if (!isEmpty(h1) && !isEmpty(h2)) {
      val minValH1 = findMin(h1)
      val minValH2 = findMin(h2)
      val heapMe = meld(h1, h2)
      val meldedMin = findMin(heapMe)
      meldedMin == getMinValue(minValH1, minValH2)
    } else true
  }

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  property("MeldYOrdenar") = forAll { (heaps: (H, H)) =>
    val l1 = createOrderListByHeap(heaps._1)
    val l2 = createOrderListByHeap(heaps._2)
    val melded = meld(heaps._1, heaps._2)
    val lMelded = createOrderListByHeap(melded)
    (l1 ++ l2).sorted == lMelded
  }

  def createHeap(list: List[Int], h: H): H = {
    list match {
      case Nil => h
      case x :: xs => createHeap(xs, insert(x, h))
    }
  }
  def getMinValue(a: Int, b: Int) = if (a > b) b else a

  def createOrderListByHeap(heap: H): List[Int] = {
    if (isEmpty(heap)) {
      Nil
    } else {
      findMin(heap) :: createOrderListByHeap(deleteMin(heap))
    }
  }
  def orderListElement(listDes: List[Int], listOrd: List[Int]): List[Int] = {
    listDes match {
      case Nil => listOrd
      case x :: xs => listOrd match {
        case Nil => orderListElement(xs, x :: listOrd)
        case y :: ys =>
          if (x > y) orderListElement(xs, x :: y :: ys)
          else orderListElement(xs, y :: x :: ys)
      }
    }
  }

}