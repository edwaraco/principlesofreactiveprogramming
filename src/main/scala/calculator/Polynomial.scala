package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
    c: Signal[Double]): Signal[Double] = {
    Signal {
      require(a() != 0, "a is a zero value")
      val aValue = a()
      val bValue = b()
      val cValue = c()
      Math.pow(bValue, 2) - 4 * aValue * cValue
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
    c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      require(a() != 0, "a is a zero value")
      val deltaValue = delta()
      if (deltaValue < 0) Set()
      else {
        val bValue = b()
        val aValue = a()
        Set((-bValue + Math.sqrt(deltaValue)) / (2 * aValue), (-bValue - Math.sqrt(deltaValue)) / (2 * aValue))
      }
    }
  }
}
