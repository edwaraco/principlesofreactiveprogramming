package calculator

sealed abstract class Expr
final case class Literal(v: Double) extends Expr
final case class Ref(name: String) extends Expr
final case class Plus(a: Expr, b: Expr) extends Expr
final case class Minus(a: Expr, b: Expr) extends Expr
final case class Times(a: Expr, b: Expr) extends Expr
final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {

  def computeValues(namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] = {
    for ((k, v) <- namedExpressions) yield (k, Signal(eval(v(), namedExpressions)))
  }
  
  def existReference(cellRef: String, expr: Expr, mapReference: Map[String, Signal[Expr]]): Boolean = {
    expr match {
      case Literal(value) => false
      case Ref(name) => if (name == cellRef) true else existReference(cellRef, getReferenceExpr(name, mapReference), mapReference)
      case Plus(valueA, valueB) => existReference(cellRef, valueA, mapReference) || existReference(cellRef, valueB, mapReference)
      case Minus(valueA, valueB) => existReference(cellRef, valueA, mapReference) || existReference(cellRef, valueB, mapReference)
      case Times(valueA, valueB) => existReference(cellRef, valueA, mapReference) || existReference(cellRef, valueB, mapReference)
      case Divide(valueA, valueB) => existReference(cellRef, valueA, mapReference) || existReference(cellRef, valueB, mapReference)
    }
  }
  
  def eval(expr: Expr, references: Map[String, Signal[Expr]]): Double = {
    expr match {
      case Literal(value) => value
      case Plus(valueA, valueB) => eval(valueA, references) + eval(valueB, references)
      case Minus(valueA, valueB) => eval(valueA, references) - eval(valueB, references)
      case Times(valueA, valueB) => eval(valueA, references) * eval(valueB, references)
      case Divide(valueA, valueB) => {
        val valB = eval(valueB, references)
        //if (valB != 0) 
          eval(valueA, references) / valB 
        //else Double.NaN
      }
        
      case Ref(name) => if (existReference(name, getReferenceExpr(name, references), references)) Double.NaN else eval(getReferenceExpr(name, references), references)
    }
  }

  /**
   * Get the Expr for a referenced variables.
   *  If the variable is not known, returns a literal NaN.
   */
  private def getReferenceExpr(name: String,
    references: Map[String, Signal[Expr]]) = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}
