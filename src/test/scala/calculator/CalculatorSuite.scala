package calculator

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest._
import TweetLength.MaxTweetLength
import java.util.Calendar

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers {

  /**
   * ****************
   * * TWEET LENGTH **
   * ****************
   */

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }

  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  test("colorForRemainingCharsCount with a changing signal") {
    val cantCaracteres = Var(-5)
    val resultOrange3 = TweetLength.colorForRemainingCharsCount(cantCaracteres)
    cantCaracteres() = 5
    assert(resultOrange3() == "orange")
  }

  test("computeDelta with a Constant Values for a, b, c") {
    val resultDelta1 = Polynomial.computeDelta(Var(1.0), Var(4.0), Var(3.0))
    assert(resultDelta1() == 4.0)
  }

  test("computeDelta with a Changing Values for a, b, c") {
    val aValue = Var(1.0)
    val bValue = Var(0.0)
    val cValue = Var(0.0)
    val resultDelta1 = Polynomial.computeDelta(aValue, bValue, cValue)
    aValue() = 1.0
    assert(resultDelta1() == 0.0)
    bValue() = 4.0
    assert(resultDelta1() == 16.0)
    cValue() = 3.0
    assert(resultDelta1() == 4.0)
    cValue() = 4.0
    assert(resultDelta1() == 0)
  }
  
  test("computeSolutions with a Constant Values for a, b, c") {
    val aValue = Signal(1.0)
    val bValue = Signal(4.0)
    val cValue = Signal(3.0)
    val deltaValue = Polynomial.computeDelta(aValue, bValue, cValue)
    val resultDelta1 = Polynomial.computeSolutions(aValue, bValue, cValue, deltaValue)
    assert(resultDelta1() == Set(-1.0, -3.0))
    
    val aValue2 = Signal(9.0)
    val bValue2 = Signal(18.0)
    val cValue2 = Signal(9.0)
    val deltaValue2 = Polynomial.computeDelta(aValue2, bValue2, cValue2)
    val resultDelta2 = Polynomial.computeSolutions(aValue2, bValue2, cValue2, deltaValue2)
    assert(resultDelta2() == Set(-1.0))
  
    
    val aValue3 = Signal(1.0)
    val bValue3 = Signal(4.0)
    val cValue3 = Signal(9.0)
    val deltaValue3 = Polynomial.computeDelta(aValue3, bValue3, cValue3)
    assert(deltaValue3() < 0.0)
    val resultDelta3 = Polynomial.computeSolutions(aValue3, bValue3, cValue3, deltaValue3)
    assert(resultDelta3() == Set())
  }
  
  test("eval function with simples calculates in cells") {
    val literal1 = Literal(5)
    val literal2 = Literal(1)
    val cellA = Plus(literal1, literal2)
    val cellB = Minus(literal1, literal2)
    val cellC = Times(literal1, literal2)
    val cellD = Divide(literal1, literal2)
    assert(5 == Calculator.eval(literal1, null))
    assert(1 == Calculator.eval(literal2, null))
    assert(6 == Calculator.eval(cellA, null))
    assert(4 == Calculator.eval(cellB, null))
    assert(5 == Calculator.eval(cellC, null))
    assert(5 == Calculator.eval(cellD, null))
  }
  
  test("eval function with calculates between the cells") {
    val literal1 = Literal(5)
    val literal2 = Literal(1)
    val cellA = Plus(literal1, literal2)
    val strCellA = "a"
    val refCellA = Ref(strCellA)
    val cellB = Minus(literal1, refCellA)
    val cellC = Times(literal1, refCellA)
    val cellD = Divide(literal1, refCellA)
    val mapReferences: Map[String, Signal[Expr]] = Map(strCellA -> Signal(cellA))
    assert(6 == Calculator.eval(cellA, mapReferences))
    assert(-1 == Calculator.eval(cellB, mapReferences))
    assert(30 == Calculator.eval(cellC, mapReferences))
    val result = 5.0 / 6.0
    assert(result == Calculator.eval(cellD, mapReferences))
  }
  
  test("eval function with calculates between the cells within cyclic references") {
    val literal1 = Literal(5)
    val literal2 = Literal(1)
    val cellA = Plus(literal1, literal2)
    val strCellA = "a"
    val strCellB = "b"
    val strCellC = "c"
    val refCellA = Ref(strCellA)
    val refCellC = Ref(strCellB)
    val refCellB = Ref(strCellC)
    
    val cellB = Minus(refCellC, refCellA)
    val cellC = Times(refCellB, refCellA)
    val mapReferences: Map[String, Signal[Expr]] = Map(strCellA -> Signal(cellA),strCellB -> Signal(cellB),strCellB -> Signal(cellB))
    assert(6 == Calculator.eval(cellA, mapReferences))
    assert(Double.NaN.equals(Calculator.eval(cellB, mapReferences)))
  }
  
  test("computeValues function with calculates between the cells without cyclic references") {
    var namedExpressions: Map[String, Signal[Expr]] = Map("a1" -> Signal(Literal(1.0)))
    for( i <- 2 to 10 ){
	   namedExpressions = namedExpressions + ("a"+ i -> Signal(Plus(Ref("a"+ (i-1)), Literal(i.toDouble))))
	}
    val computeValue = Calculator.computeValues(namedExpressions)
    assert(1.0 == computeValue("a1")())
    assert(3.0 == computeValue("a2")())
    assert(6.0 == computeValue("a3")())
    assert(10.0 == computeValue("a4")())
    assert(15.0 == computeValue("a5")())
    assert(21.0 == computeValue("a6")())
    assert(28.0 == computeValue("a7")())
    assert(36.0 == computeValue("a8")())
    assert(45.0 == computeValue("a9")())
    assert(55.0 == computeValue("a10")())
  }
  
  test("computeValues function with calculates between the cells without cyclic references: expect the values recomputed as a result are not highlighted for 1.5s") {
    var namedExpressions: Map[String, Signal[Expr]] = Map("a1" -> Signal(Literal(1.0)))
    for( i <- 2 to 100 ){
	   namedExpressions = namedExpressions + ("a"+ i -> Signal(Plus(Ref("a"+ (i-1)), Literal(i.toDouble))))
	}
    val time = Calendar.getInstance().getTimeInMillis()
    val computeValue = Calculator.computeValues(namedExpressions)
    val timeRecomputed = Calendar.getInstance().getTimeInMillis() - time 
    assert(1500 > timeRecomputed)
  }
}
