package nodescala

import scala.language.postfixOps
import scala.util.{ Try, Success, Failure }
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{ async, await }
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be completed") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }
  test("A Future should never be completed") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }
  test("Future.all all futures passeds") {
    val fs = List(Future.always(517), Future.always(518))
    val all = Future.all(fs)
    val res = Await.result(all, 1 second)
    assert(res === List(517, 518))
  }

  test("Future.all a second futures failed") {
    val fs = List(Future.always(517), Future.never)
    val all = Future.all(fs)
    intercept[TimeoutException](Await.result(all, 1 second))
  }

  test("Future.any return a future when all passed") {
    val fs = List(Future.always(517), Future.always(517))
    val all = Future.any(fs)
    assert(Await.result(all, 1 second) == 517)
  }

  test("Future.any return a future when someone failed") {
    val fs = List(Future.never, Future.always(517))
    val all = Future.any(fs)
    assert(Await.result(all, 1 second) == 517)
  }

  test("Future.any return someone future when someone failed [more than 2 futures at the list]") {
    val fs = List(Future.never, Future.always(517), Future.always(518))
    val all = Future.any(fs)
    val result = Await.result(all, 1 second);
    assert(result == 517 || result == 518)
  }

  test("Future.delay complete the future before 2 seconds") {
    val all = Future.delay(2 second)
    Await.result(all, 3 second)
    assert(true)
  }

  test("Future.delay not complete the future after 2 seconds") {
    val all = Future.delay(2 second)
    intercept[TimeoutException](Await.result(all, 1 second))

  }

  test("Future.now should be complete") {
    val always = Future.always(517)
    assert(always.now === 517)
  }

  test("Future.now should throw an exception if it's not completed") {
    val never = Future.never
    intercept[NoSuchElementException](never.now)
  }

  test("Future.continueWith should be \"success\" value after execute Future.always method ") {
    val future = Future.always(1).continueWith { _ => "success" }
    val res = Await.result(future, 1 second)
    assert(res === "success")
  }

  test("Future.continueWith should be \"success\" value after execute Future.always method that throw a Exception ") {
    val future = (Future[String] { throw new Exception }).continueWith(_ => "success")
    val res = Await.result(future, 1 second)
    assert(res === "success")
  }

  test("Future.continueWith should throw Exception after execute Future.always method") {
    val future1 = Future.always(1)
    val future = future1.continueWith(_ => throw new Exception())
    intercept[Exception](Await.result(future, 1 second))
  }
  
  test(" Future.continue should contain \"success\"  after execute Future.always method") {
    val future = Future.always(1).continue { _ => "success" }
    val res = Await.result(future, 1 second)
    assert(res === "success")
  }

  test("Future.continue should throw Exception  after execute Future.always method") {
    val future = Future.always("hello").continue { _ => throw new Exception }
    intercept[Exception](Await.result(future, 1 second))
  }
  
  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }
  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




