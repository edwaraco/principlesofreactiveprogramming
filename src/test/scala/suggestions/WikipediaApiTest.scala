package suggestions

import language.postfixOps
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Try, Success, Failure }
import rx.lang.scala._
import org.scalatest._
import gui._

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class WikipediaApiTest extends FunSuite {

  object mockApi extends WikipediaApi {
    def wikipediaSuggestion(term: String) = Future {
      if (term.head.isLetter) {
        for (suffix <- List(" (Computer Scientist)", " (Footballer)")) yield term + suffix
      } else {
        List(term)
      }
    }
    def wikipediaPage(term: String) = Future {
      "Title: " + term
    }
  }

  import mockApi._

  test("WikipediaApi should make the stream valid using sanitized") {
    val notvalid = Observable.just("erik", "erik meijer", "martin", "Reactive proggramming")
    val valid = notvalid.sanitized

    var count = 0
    var completed = false

    val sub = valid.subscribe(
      term => {
        assert(term.forall(_ != ' '))
        count += 1
      },
      t => assert(false, s"stream error $t"),
      () => completed = true)
    assert(completed && count == 4, "completed: " + completed + ", event count: " + count)
  }

  test("WikipediaApi recovered: given a request stream: 1,2,3,Exception,5 when make recoverd we get Success[1], Success[2], Success[3], Failure[new Exception]") {
    val exception = new Exception
    val requests = Observable.just(1, 2, 3) ++ Observable.error(exception) ++ Observable.just(5)
    val expected = List(Success(1), Success(2), Success(3), Failure(exception))
    val result = requests.recovered.toBlocking.toList
    assert(result.size === expected.size)
    assert(result(0) === expected(0))
    assert(result(1) === expected(1))
    assert(result(2) === expected(2))
    assert(result(3) === expected(3))
  }

  test("WikipediaApi timedOut: given a interval of 0.1 second, when timeoud is 1 second then should 9 items") {
    val clock = Observable.interval(0.1 second)
    val timedOut = clock.timedOut(1)
    assert(timedOut.toBlocking.toList.length === 9)
  }

  test("WikipediaApi should correctly use concatRecovered") {
    val requests = Observable.just(1, 2, 3)
    val remoteComputation = (n: Int) => Observable.just(0 to n: _*)
    val responses = requests concatRecovered remoteComputation
    val sum = responses.foldLeft(0) { (acc, tn) =>
      tn match {
        case Success(n) => acc + n
        case Failure(t) => throw t
      }
    }
    var total = -1
    val sub = sum.subscribe {
      s => total = s
    }
    assert(total == (1 + 1 + 2 + 1 + 2 + 3), s"Sum: $total")
  }

}
